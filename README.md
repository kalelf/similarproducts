# SimilarProductsApi
Development of an API that provides the product detail of the similar products for a given one, in order to offer a new feature to our customers showing similar products to the one they are currently seeing.
### Building And Running the Service
The service build is based on `maven`. In order to build it runs:    
```bash prompt> .\mvnw package ``` if you are in Linux OS or in the IDEA Intellij terminal. In case of using Windows you need to use the following command ```bash prompt> mvnw.cmd package ``` In order to build it you need Java JDK 11 (both Oracle JDK and OpenJDK are valid).

Once built, `maven` creates a jar file containing the API and all of its dependencies (thanks to the spring-boot maven plugin). 
In order to run the API, execute: `java -jar target/SimilarProductsApi-0.0.1-SNAPSHOT.jar`.  You need Java Runtime Environment 11 (Oracle JRE or OpenJDK JRE).

It is also possible to directly run the service using maven: `.\mvnw spring-boot:run` in Linux or `mvnw.cmd spring-boot:run`

# Swagger
The Api includes Swagger specification, when the application runs, you can access to `http://localhost:5000/swagger-ui/` and see all Api documentation.

![img_1.png](img_1.png)

## Configuration    

In addition, as the standard Spring Boot properties framework is used, all the common properties (https://docs.spring.io/spring-boot/docs/current/reference/html/common-application-properties.html) can be used in the service as well. For example, it is possible to change the port in which the service will listen (5000 by default).

The following is the list of most important common configuration keywords used by the API:

| Keyword  | Description  | Possible Values | Default Value |
|---|---|---|---|
| server.port | The port at which the API is going to listen for request. | Positive integer between 1 and 65535. | 5000 |
| api.info.version | Api version. This version will appears in the swagger file| X.Y.Z X=mayorVersion, Y=minorVersion, Z= buildVersion | 1.0.0 |
| api.configuration.dev.mode | if true the swagger info will be able to be showed, else not | true or false | true |
| http-pool.max-total | The maximum support connection of the entire connection pool | Positive integer | 200 |
| http-pool.default-max-per-route | The maximum number of connections allowed for a route that has not been specified otherwise by a call to setMaxPerRoute | Positive integer | 100 |
| http-pool.connection-timeout | Timeout time to establish a connection with the target host | Positive integer | 30000 |
| http-pool.connection-request-timeout | Get the connection timeout from the poolnettpclientConnectionManager | Positive integer | 30000 |
| http-pool.socket-timeout | Read data timeout from the destination host | Positive integer | 60000 |
