package com.backend.dev.test.similarproducts.configuration;

import org.apache.http.client.HttpClient;
import org.apache.http.client.config.RequestConfig;
import org.apache.http.impl.client.HttpClientBuilder;
import org.apache.http.impl.conn.PoolingHttpClientConnectionManager;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.client.ClientHttpRequestFactory;
import org.springframework.http.client.HttpComponentsClientHttpRequestFactory;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.util.DefaultUriBuilderFactory;

import java.time.Duration;

@Configuration
public class RestTemplateConfig {

    @Value("${api.configuration.existingApiURL}")
    private String existingApiURL;

    @Autowired
    private HttpPoolProperties poolProperties;

    @Bean
    public RestTemplate restTemplate() {
        var template = new RestTemplate(httpRequestFactory());
        template.setUriTemplateHandler(new DefaultUriBuilderFactory(existingApiURL));
        return template;
    }

    @Bean
    public ClientHttpRequestFactory httpRequestFactory(){
        return new HttpComponentsClientHttpRequestFactory(httpClient());
    }

    @Bean
    public HttpClient httpClient(){

        return  HttpClientBuilder.create()
                .setConnectionManager(poolingHttpClientConnectionManager())
                .setDefaultRequestConfig(requestConfig()).build();
    }

    @Bean
    public PoolingHttpClientConnectionManager poolingHttpClientConnectionManager(){
        var poolingHttpClientConnectionManager = new PoolingHttpClientConnectionManager();
        poolingHttpClientConnectionManager.setMaxTotal(poolProperties.getMaxTotal());
        poolingHttpClientConnectionManager.setDefaultMaxPerRoute(poolProperties.getDefaultMaxPerRoute());

        return poolingHttpClientConnectionManager;
    }

    @Bean
    public RequestConfig requestConfig(){
        return RequestConfig
                .custom()
                .setConnectionRequestTimeout(poolProperties.getConnectionRequestTimeout()) // timeout to get connection from pool
                .setSocketTimeout(poolProperties.getSocketTimeout()) // standard connection timeout
                .setConnectTimeout(poolProperties.getConnectionTimeout()) // standard connection timeout
                .build();
    }

}
