package com.backend.dev.test.similarproducts.configuration;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import springfox.documentation.builders.ApiInfoBuilder;
import springfox.documentation.builders.PathSelectors;
import springfox.documentation.builders.RequestHandlerSelectors;
import springfox.documentation.service.ApiInfo;
import springfox.documentation.spi.DocumentationType;
import springfox.documentation.spring.web.plugins.Docket;

@javax.annotation.Generated(value = "io.swagger.codegen.v3.generators.java.SpringCodegen", date = "2020-04-29T13:08:29.073Z[GMT]")
@Configuration
public class SwaggerDocumentationConfig {

    @Autowired
    private ApiConfig apiConfig;

    ApiInfo apiInfo() {
        return new ApiInfoBuilder()
                .title("SimilarProductsApi")
                .description("### Description\n Development of an API that provides the product detail of the similar products for a given one, " +
                        "in order to offer a new feature to our customers showing similar products to the one they are currently seeing.")
                .version(apiConfig.getVersion())
                .build();
    }

    @Bean
    public Docket customImplementation() {
        return new Docket(DocumentationType.SWAGGER_2)
                .select()
                .apis(RequestHandlerSelectors.basePackage("com.backend.dev.test.similarproducts.controller"))
                .paths(PathSelectors.any())
                .build()
                .enable(apiConfig.getDevMode())
                .apiInfo(apiInfo());
    }


}
