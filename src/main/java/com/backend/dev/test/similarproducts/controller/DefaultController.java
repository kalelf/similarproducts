package com.backend.dev.test.similarproducts.controller;

import com.backend.dev.test.similarproducts.api.DefaultApi;
import com.backend.dev.test.similarproducts.model.ProductDetail;
import com.backend.dev.test.similarproducts.service.ProductSimilarService;
import io.swagger.annotations.ApiParam;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
public class DefaultController implements DefaultApi {

    @Autowired
    private ProductSimilarService productSimilarService;

    @Override
    public ResponseEntity<List<ProductDetail>> getProductSimilar(@ApiParam(value = "",required=true) @PathVariable("productId") String productId){
        return ResponseEntity.ok(productSimilarService.getSimilarProducts(productId));
    }
}

