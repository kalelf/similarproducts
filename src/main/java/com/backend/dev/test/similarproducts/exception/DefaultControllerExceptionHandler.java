package com.backend.dev.test.similarproducts.exception;

import org.apache.http.conn.ConnectionPoolTimeoutException;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.client.HttpClientErrorException;
import org.springframework.web.client.HttpServerErrorException;

import java.net.SocketTimeoutException;

@ControllerAdvice
public class DefaultControllerExceptionHandler {

    @ExceptionHandler
    public ResponseEntity<Object> handleNotFoundException(HttpClientErrorException exc){
        return ResponseEntity.status(HttpStatus.NOT_FOUND).build();
    }

    @ExceptionHandler
    public ResponseEntity<Object> handleInternalServerException(SocketTimeoutException exc){
        return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(exc.getMessage());
    }
    @ExceptionHandler
    public ResponseEntity<Object> handleConnectionPoolTimeoutException(ConnectionPoolTimeoutException exc){
        return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(exc.getMessage());
    }
}
