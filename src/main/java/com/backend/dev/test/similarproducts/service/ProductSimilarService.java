package com.backend.dev.test.similarproducts.service;


import com.backend.dev.test.similarproducts.model.ProductDetail;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.*;
import org.springframework.stereotype.Service;
import org.springframework.web.client.HttpStatusCodeException;
import org.springframework.web.client.RestTemplate;

import java.net.SocketTimeoutException;
import java.util.*;

@Service
public class ProductSimilarService {

    private static Logger logger = LoggerFactory.getLogger(ProductSimilarService.class);

    @Autowired
    private RestTemplate restTemplate;

    public List<ProductDetail> getSimilarProducts(String productId) {
        List<ProductDetail> similarProductsDetail = new ArrayList<>();

        var similarProductIds = getSimilarProductIds(productId);

        for (var similarProduct : similarProductIds) {
            var productDetail = getProductDetail(similarProduct);

            if(productDetail != null)
                similarProductsDetail.add(productDetail);
        }

        return similarProductsDetail;
    }

    private String[] getSimilarProductIds(String productId) {
        return restTemplate.exchange("/product/{productId}/similarids", HttpMethod.GET,
                                                new HttpEntity<String>(getHttpHeaders()), String[].class, getHttpParams(productId))
                                            .getBody();
    }

    private ProductDetail getProductDetail(String productId){

        ProductDetail productDetail = null;

        try{
            productDetail = restTemplate.exchange("product/{productId}", HttpMethod.GET, 
                    new HttpEntity<String>(getHttpHeaders()), ProductDetail.class, getHttpParams(productId))
                    .getBody();
        }
        catch (HttpStatusCodeException ex){
            if(ex.getStatusCode().is4xxClientError())
                logger.error("There was an error retrieving some data: The product with id "+ productId + " was not found");

            if (ex.getStatusCode().is5xxServerError())
                logger.error("There was an error retrieving some data: There was a problem retrieving the details for the productId " + productId);
        }
        return productDetail;
    }


    private Map<String, String> getHttpParams(String productId) {
        Map<String, String> params = new HashMap<String, String>();
        params.put("productId", productId);
        return params;
    }

    private HttpHeaders getHttpHeaders() {
        HttpHeaders headers = new HttpHeaders();
        headers.setAccept(Arrays.asList(MediaType.APPLICATION_JSON));
        return headers;
    }


}
