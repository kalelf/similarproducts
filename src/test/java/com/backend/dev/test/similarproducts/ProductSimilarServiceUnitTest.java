package com.backend.dev.test.similarproducts;

import com.backend.dev.test.similarproducts.model.ProductDetail;
import com.backend.dev.test.similarproducts.service.ProductSimilarService;
import org.junit.Assert;
import org.junit.Test;
import org.junit.platform.commons.logging.Logger;
import org.junit.platform.commons.logging.LoggerFactory;
import org.junit.runner.RunWith;
import org.mockito.ArgumentMatchers;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.MockitoJUnitRunner;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.client.HttpClientErrorException;
import org.springframework.web.client.RestTemplate;

import java.math.BigDecimal;
import java.util.List;

@RunWith(MockitoJUnitRunner.class)
public class ProductSimilarServiceUnitTest {

    private static final Logger logger = LoggerFactory.getLogger(ProductSimilarServiceUnitTest.class);

    @Mock
    private RestTemplate restTemplate;

    @InjectMocks
    private ProductSimilarService productSimilarService = new ProductSimilarService();

    @Test
    public void givenProductId_whenGetSimilarProducts_shouldReturnAProductDetailList(){
        //Employee emp = new Employee("E001", "Eric Simmons");

        String[] ids = {"1"};

        mockGetSimilarIds(ids);

        ProductDetail productDetail = getProductDetail();

        mockGetProductDetail(productDetail);

        var list = productSimilarService.getSimilarProducts("1");


        Assert.assertEquals(list.size(), 1);
        Assert.assertSame(list.stream().findFirst().get(), productDetail);
        Assert.assertTrue(list.contains(productDetail));
    }

    @Test
    public void givenProductId_whenGetSimilarProducts_shouldThrowNotFoundExceptionAndEmptyList() {
        String[] ids = {"1"};

        mockGetSimilarIds(ids);

        mockGetProductDetailNotFound();

        var list = productSimilarService.getSimilarProducts("1");

        Assert.assertTrue(list.isEmpty());
    }

    @Test
    public void givenProductId_whenGetSimilarProducts_shouldReturnInteralErrorAndEmptyList() {
        String[] ids = {"1"};

        mockGetSimilarIds(ids);

        mockGetProductDetailInteralError();

        var list = productSimilarService.getSimilarProducts("1");

        Assert.assertTrue(list.isEmpty());
    }


    private void mockGetProductDetail(ProductDetail productDetail) {
        var productDetailResponse = new ResponseEntity<>(productDetail, HttpStatus.OK);

        Mockito.when(restTemplate.exchange(Mockito.eq("product/{productId}"),
                        Mockito.<HttpMethod> any(),
                        Mockito.<HttpEntity<String>> any(),
                        Mockito.<Class<ProductDetail>> any(),
                        Mockito.<String, Object> anyMap()))
                .thenReturn(productDetailResponse);
    }
    private void mockGetProductDetailNotFound() {

        Mockito.when(restTemplate.exchange(Mockito.eq("product/{productId}"),
                        Mockito.<HttpMethod> any(),
                        Mockito.<HttpEntity<String>> any(),
                        Mockito.<Class<String>> any(),
                        Mockito.<String, Object> anyMap()))
                .thenThrow(new HttpClientErrorException(HttpStatus.NOT_FOUND, "Product not found"));
    }
    private void mockGetProductDetailInteralError() {

        Mockito.when(restTemplate.exchange(Mockito.eq("product/{productId}"),
                        Mockito.<HttpMethod> any(),
                        Mockito.<HttpEntity<String>> any(),
                        Mockito.<Class<String>> any(),
                        Mockito.<String, Object> anyMap()))
                .thenThrow(new HttpClientErrorException(HttpStatus.INTERNAL_SERVER_ERROR));
    }

    private void mockGetSimilarIds(String[] ids) {
        var similarIdsResponse = new ResponseEntity<>(ids, HttpStatus.OK);

        Mockito.when(restTemplate.exchange(Mockito.eq("/product/{productId}/similarids"),
                        Mockito.<HttpMethod> any(),
                        Mockito.<HttpEntity<String>> any(),
                        Mockito.<Class<String[]>> any(),
                        Mockito.<String, Object> anyMap()))
                .thenReturn(similarIdsResponse);
    }

    private ProductDetail getProductDetail() {
        var productDetail = new ProductDetail();
        productDetail.setId("1");
        productDetail.setName("Jeans");
        productDetail.setAvailability(true);
        productDetail.setPrice(new BigDecimal(10));
        return productDetail;
    }



}
